
## Simple Blog App
a simple project with Laravel for backend and VueJS for frontend.

### General Structure
**/api** - run with `php artisan serve`
Laravel app, written to serve the REST APIs for public consumption


**/ui** - execute with `npm run dev` for development & `http-server dist\` for testing production
VueJS app to serve independent ui dashboard that consumes backend APIs

### API Guide
- register new use
```sh
url: /api/register
method: POST
headers: {
    "Content-Type": "application/json",
    "Accept": "application/json"
}
data: {
    "email": "adean6@email.com",
    "name": "adean6",
    "password": "supersecret",
    "password_confirmation": "supersecret"
}
```

- login new use
```sh
url: /api/login
method: POST
headers: {
    "Content-Type": "application/json",
    "Accept": "application/json"
}
data: {
	"email": "adean2@email.com",
	"password": "supersecret"
}
```

- check user session
```sh
url: /api/user
method: GET
headers: {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer <tokken>"
}
```

- login new user
```sh
url: /api/logout
method: POST
headers: {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer <tokken>"
}
data: {
	"email": "adean2@email.com",
	"password": "supersecret"
}
```

- list blog articles by 10 per request
- you may filter by author and title or title substring
```sh
url: /api/blogs/by/{author}/{page}/{title or title substring}
method: GET
headers: {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer <tokken>"
}
```

- fetch blog by article id
```sh
url: /api/blogs/{id}
method: GET
headers: {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer <tokken>"
}
```

- add new article
```sh
url: /api/blogs
method: POST
headers: {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer <tokken>"
}
data: {
	"title": "<article title>",
	"author": "<author name>",
	"content": "lorem ipsum...",
	"banner_img": "<url of image>",
	"status": "draft / published"
}
```

- update existing article found by article id
```sh
url: /api/blogs/{id}
method: PUT
headers: {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer <tokken>"
}
data: {
	"title": "<article title>",
	"author": "<author name>",
	"content": "lorem ipsum...",
	"banner_img": "<url of image>",
	"status": "draft / published"
}
```

- delete existing article found by article id
```sh
url: /api/blogs/{id}
method: DELETE
headers: {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer <tokken>"
}
```