import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import ArticleList from '@/components/ArticleList'
import AddArticle from '@/components/AddArticle'
import ArticlePage from '@/components/ArticlePage'
import EditArticle from '@/components/EditArticle'

Vue.use(Router)

export default new Router({
	routes: [
		{
			path: '/',
			name: 'login',
			component: Login
		},
		{
			path: '/articles',
			name: 'articleList',
			component: ArticleList
		},
		{
			path: '/article/view/:blog',
			name: 'articlePage',
			component: ArticlePage
		},
		{
			path: '/article/add',
			name: 'addArticle',
			component: AddArticle
		},
		{
			path: '/article/edit/:blog',
			name: 'editArticle',
			component: EditArticle
		}
	]
})
