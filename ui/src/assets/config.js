export default {
	api: 'http://localhost:8000/api',
	headers: {
		'Content-type': 'application/json',
		Accepts: 'application/json',
		Authorization: 'Bearer ' + localStorage.getItem('auth')
	}
}
