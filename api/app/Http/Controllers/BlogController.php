<?php

namespace App\Http\Controllers;
use App\Blog;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index($author, $page, $title)
    {
        $limit = 10;
        $offset = ($page-1) * $limit;
        error_log('find: '.$author.', page: '.$page.', offset: '.$offset.', subs-title:'.$title);

        if ($author == '*'){
            if ($title == '*'){
                return Blog::skip($offset)->take($limit)->get();
            }
            else {
                return Blog::where('title', 'like', '%'.$title.'%')->skip($offset)->take($limit)->get(); }
            }
        else {
            if ($title == '*'){
                return Blog::where('author', $author)
                        ->skip($offset)
                        ->take($limit)
                        ->get();
            }
            else {
                return Blog::where([
                            ['author', $author],
                            ['title', 'like', '%'.$title.'%']
                        ])
                        ->skip($offset)
                        ->take($limit)
                        ->get();
            }
        }
    }

    public function show($id)
    {
        return Blog::findOrFail($id);
    }

    public function store(Request $request)
    {
        $blog = Blog::create($request->all());

        return response()->json($blog, 201);
    }

    public function update(Request $request, $id) // $id --> fetch from reqest params
    {
        $blog = Blog::findOrFail($id);
        $blog->update($request->all()); // fetch from request 'data'

        return response()->json($blog, 200);
    }

    public function delete($id)
    {
        $blog = Blog::findOrFail($id);
        $blog->delete();

        return array('id' => $id);
    }
}
