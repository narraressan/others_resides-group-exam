<?php

use Illuminate\Http\Request;
Use App\Article;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	$data = array(
		'bearer' => $request->user(),
		'sign' => time()
	);
    return $data;
});

Route::group(['middleware' => 'auth:api'], function() {
	Route::get('blogs/by/{author}/{page}/{title}', 'BlogController@index');
	Route::get('blogs/{id}', 'BlogController@show');
	Route::post('blogs', 'BlogController@store');
	Route::put('blogs/{id}', 'BlogController@update');
	Route::delete('blogs/{id}', 'BlogController@delete');
});

Route::middleware('cors')->post('register', 'Auth\RegisterController@register');
Route::middleware('cors')->post('login', 'Auth\LoginController@login');
Route::middleware('cors')->post('logout', 'Auth\LoginController@logout');